package com.tangyiyi.pgc;


import java.util.ArrayList;
import java.util.Arrays;


class Value {
    private String val;
    private int repeats;

    public Value() {
        val = null;
        repeats = 0;
    }

    public Value(String v, int rep) {
        val = v;
        repeats = rep;
    }

    public void increment(int i) {
        repeats += i;
    }

    public boolean equals(Value other) {
        return this.val.equals(other.val) ;
    }

    public int count() {
        return repeats;
    }

    public String name() {
        return val;
    }
}

class DataRow {
    private String name;

    private ArrayList<Value> values = new ArrayList<>();

    public DataRow(ArrayList<String> strArr) {
        name = strArr.get(0);
        for (int i = 1; i < strArr.size(); ++i) {
            Value v = new Value(strArr.get(i), 1);
            if (i > 1) {
                Value last = values.get(values.size()-1);
                if (last.equals(v)) {
                    last.increment(1);
                } else {
                    values.add(v);
                }
            }
            else {
                values.add(v);
            }
        }
    }

    public String print() {
        String res = "<tr>";
        res += "<td>" + name + "</td>";
        for (int i = 0; i < values.size(); ++i) {
            Value v = values.get(i);
            if (v.count() > 1) {
                res += "<td colspan=\"" + v.count() + "\">";
            } else {
                res += "<td>";
            }
            res += v.name();
            res += "</td>";

        }
        res += "</tr>";

        return res;
    }
}


public class Main {

    private static String str = "Brackets	Plug-in	Yes	Plug-in	No	Yes	Yes\n" +
            "Coda	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "ConTEXT	No	Partial	Partial	Yes	Yes	Yes \n" +
            "Diakonos	Yes	Yes	No	No	Yes	Yes \n" +
            "ed	No	Yes	No	No	No	No \n" +
            "EditPlus	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Editra	Yes	Yes	?	?	?	Yes \n" +
            "EmEditor	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Geany	Plug-in	Yes	Yes	Yes	Yes	Yes \n" +
            "gedit	Yes	Plug-in	Yes	Yes	Yes	Plug-in \n" +
            "GNU Emacs	Plug-in	Yes	Yes	Yes	Yes	Yes \n" +
            "Gobby	No	No	Partial	Partial	No	No \n" +
            "JED	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "JOVE	Yes	Yes	No	No	Yes	Yes \n" +
            "Kate	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "KEDIT	No	Yes	No	Yes	Yes	Yes \n" +
            "Komodo Edit	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "KWrite	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "LE	No	Yes	No	Yes	Yes	Yes \n" +
            "Light Table	Plug-in	?	No	No	Yes	? \n" +
            "Metapad	Partial	No	Yes	Yes	Yes	No \n" +
            "mg	No	Yes	No	Partial	Yes	No \n" +
            "MinEd	No	Yes	Yes	Yes	No	Yes \n" +
            "MS-DOS Editor	No	No	No	Yes	No	No \n" +
            "Nano	Yes	Yes	No	Yes	Yes	No \n" +
            "ne	No	Yes	No	No	Yes	Yes \n" +
            "NEdit	Plug-in	Yes	No	Yes	Yes	Yes \n" +
            "Notepad	No	No	No	No	No	No \n" +
            "Notepad++	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Notepad2	No	Limited	Yes	Yes	Yes	Yes \n" +
            "NoteTab	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "nvi	No	Yes	No	No	Yes	? \n" +
            "Peppermint	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Pico	Yes	No	No	No	No	No \n" +
            "PolyEdit	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Programmer's Notepad	No	Yes	Yes	Yes	Yes	Yes \n" +
            "PSPad	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Q10	Yes	?	?	?	?	? \n" +
            "RText	Yes	Yes	Yes	Yes	Yes	No \n" +
            "SciTE	No	Limited	No	Yes	Yes	Yes \n" +
            "SlickEdit	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Smultron	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Source Insight	No	Yes	No	Yes	Yes	Yes\n" +
            "SubEthaEdit	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Sublime Text	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "TED Notepad	No	No	No	Yes	Yes	No \n" +
            "TextEdit	Yes	No	Yes	Yes	Yes	Yes \n" +
            "TextMate	Yes	Yes	Partial	Yes	Yes	Yes \n" +
            "TextPad	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "TextWrangler	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "The SemWare Editor	Yes	Yes	No	Yes	Yes	Yes \n" +
            "UltraEdit	Yes	Limited	Yes	Yes	Yes	Yes \n" +
            "VEDIT	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "vi	No	Yes	No	No	No	No \n" +
            "Vim	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "Visual Studio Code	Plug-in	Yes	Yes	Yes	Yes	Yes \n" +
            "Xeditor	Yes	Yes	Yes	Yes	Yes	Yes \n" +
            "XEmacs	Plug-in	Yes	Yes	Yes	Yes	Yes \n" +
            "Yi	?	Yes	?	?	Yes	Yes"
    ;

    public static void main(String[] args) {
	// write your code here
        System.out.println(toTable(str));
    }

    public static String toTable(String str) {

        ArrayList<String> words = new ArrayList<String>(Arrays.asList(str.split("[\t\n]")));

        ArrayList<DataRow> rows = new ArrayList<>();

        ArrayList<String> strRow = new ArrayList<String>();

        for (int i = 0; i < words.size(); ++i) {
            if (i > 0 && i % 7 == 0) {
                rows.add(new DataRow(strRow));
                strRow = new ArrayList<String>();
            }
            strRow.add(words.get(i));
            //System.out.println(row);
        }

        String result = "";

        for (int i = 0; i < rows.size(); ++i) {
            DataRow r = rows.get(i);
            result += r.print();
        }

        return result;
    }

}
